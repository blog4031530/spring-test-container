package com.example.spring.testcontainer.controller;

import com.example.spring.testcontainer.dto.MemberDto;
import com.example.spring.testcontainer.entity.Member;
import com.example.spring.testcontainer.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TestController {
	private final MemberRepository memberRepository;

	@GetMapping("/member")
	public ResponseEntity<List<Member>> listMember() {
		return ResponseEntity.ok(memberRepository.findAll());
	}

	@PostMapping("/member")
	public void createMember(@RequestBody Member member) {
		memberRepository.save(member);
	}
}
