package com.example.spring.testcontainer.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

/**
 * 공통 Entity
 */
@Getter
@ToString
@MappedSuperclass
public abstract class BaseEntity {
    /**
     * 생성 일시
     */
    @CreatedDate
    @Column(name = "create_datetime", nullable = false, updatable = false)
    protected LocalDateTime createDatetime = LocalDateTime.now();

    /**
     * 생성 계정 식별자
     */
    @CreatedBy
    @Column(name = "creator_id", nullable = false, updatable = false)
    protected Long creatorId = 1L;

    /**
     * 수정 일시
     */
    @LastModifiedDate
    @Column(name = "modify_datetime", nullable = false)
    protected LocalDateTime modifyDatetime = LocalDateTime.now();

    /**
     * 수정 계정 식별자
     */
    @LastModifiedBy
    @Column(name = "modifier_id", nullable = false)
    protected Long modifierId = 1L;
}
