package com.example.spring.testcontainer.entity;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(name = "Member")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String userName;

    @Column(name = "age", nullable = false, length = 4)
    private Integer age;

    public static Member of(Long id) {
        return new Member(id);
    }

    private Member(Long id) {
        this.id = id;
    }

    @Builder
    protected Member(Long id, String userName, Integer age) {
        this.id = id;
        this.userName = userName;
        this.age = age;
    }
}
