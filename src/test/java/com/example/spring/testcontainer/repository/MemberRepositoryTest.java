package com.example.spring.testcontainer.repository;

import com.example.spring.testcontainer.ContainerTest;
import com.example.spring.testcontainer.entity.Member;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
class MemberRepositoryTest extends ContainerTest {
	@Autowired
	private MemberRepository memberRepository;

	private final String userName = "test1";

	@Test
	void member_save_test() {
		Member member = Member.builder()
			.age(10)
			.userName(userName)
			.build();
		member = memberRepository.save(member);
		Assertions.assertThat(member.getId()).isNotNull();
	}

	@Test
	void member_search_test() {
		member_save_test();
		Optional<Member> byUserName = memberRepository.findByUserName(userName);
		Assertions.assertThat(byUserName).isPresent();
	}
}